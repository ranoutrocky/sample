package com.gokhanatil.volumeresizer;
 
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
 
import java.util.Map;
 
public class Resizer implements RequestHandler<Map<String, Object>, String> {
    public String handleRequest(Map<String, Object> input, Context context) {
 
        String result = "{'result': 'success'}";
 
        Item volumeInfo = MyDynamoDB.getVolumeInfo();
 
        if (volumeInfo != null) {
 
            String targetVolume = volumeInfo.getString("volid");
            String targetInstance = volumeInfo.getString("pip");
 
            if (VolumeChecker.isResized(targetVolume)) {
                MySSH.runShellCommands(targetInstance);
                MyDynamoDB.deleteVolumeInfo();
                result = "{'result': 'resized " + targetVolume + "'}";
            } else
                result = "{'result': 'waiting for " + targetVolume + "'}";
 
        } else VolumeChecker.checkVolumes();
 
        return result;
    }
 
}